# Teaching Collection
Web Design collections for teaching Secondary Students @MIS

Note: Please refer to non-`master` branches for teaching collections

Branches:

- [`marcom`](https://github.com/jaeyson/teaching-collection/tree/marcom) -
- [`table1`](https://github.com/jaeyson/teaching-collection/tree/table1) - 
- [`table2`](https://github.com/jaeyson/teaching-collection/tree/table2) -
- [`table3`](https://github.com/jaeyson/teaching-collection/tree/table3) -
